# BugReport <!-- omit in toc -->

## What Does This Do? <!-- omit in toc -->

![a sample form](./sample.png)

This addon:

- Allows easy creation of forms
- Has a simple system that sends the content of the form to a URL of your choice
- Appends to it some stats from the user's system

The addons are independent and can be used in isolation. This project shows an example of them in action

**Table of Contents**

- [How to Try This](#how-to-try-this)
- [Code Overview](#code-overview)
- [Todo:](#todo)

## How to Try This

There's a small test python 3 server, to use it:

```bash
python server.py
```
This server doesn't do anything; it will just log any `GET` or `POST` requests sent to it, to verify that Godot is sending the data properly. So, keep an eye on the console!

Then, run the Godot project; try entering wrong values (i.e, badly formed email), and submitting; it won't work. Then enter proper values, and submit the form again; you should see a log in the Python console with the values you entered, and the system info.

## Code Overview

If you open `Main.gd`, and `Main.tscn` you see how it works:

- Fields can be added anywhere
- Fields can take one or more validator nodes
- A `Form` node records the fields and manages submission

## Todo:

- write documentation
- automatically upload screenshot (currently does _not_ work)