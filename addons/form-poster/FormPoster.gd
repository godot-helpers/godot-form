tool
extends Node
class_name FormPoster

signal finished(response)
signal errored(response)
signal canceled()

var http = HTTPClient.new()
var timer = Timer.new()
var cancel = false
export var server_url := "127.0.0.1"
export var port := 0
export var use_https := false
export var url := "/submit"

var thread

func _ready():
	thread = Thread.new()
	timer.autostart = false
	timer.one_shot = true
	timer.connect("timeout",self,"_on_timer_timeout") 
	add_child(timer)

# Proxy to the threaded _make_post_request
func submit(data_to_send: Dictionary, wait_time:int = 10):
	cancel = false
	timer.wait_time = wait_time
	timer.start()
	thread.start(self, "_make_post_request", data_to_send)

func _on_timer_timeout():
	cancel = true
	emit_signal("canceled")

func _connect():
	if cancel: return false
	if http.connect_to_host(server_url, port, use_https) != OK:
		push_error('could not open connection')
		return false
	while (http.get_status() == HTTPClient.STATUS_CONNECTING or http.get_status() == HTTPClient.STATUS_RESOLVING) and not cancel:
		http.poll()
		OS.delay_msec(500)
	if cancel: return false
	if http.get_status() != HTTPClient.STATUS_CONNECTED:
		push_error('could not connect to host '+server_url)
		return false
	print("connected to ", server_url, ", port: ", str(port))
	return true

func _read_response():

	if not http.has_response():
		return { "headers": "", "code": 0, "text": "" }

	var headers = http.get_response_headers_as_dictionary()
	var code = http.get_response_code()
	print('code: ', code)
	print("headers:\\n", headers)

	var rb = PoolByteArray()

	while http.get_status() == HTTPClient.STATUS_BODY and not cancel:
		http.poll()
		var chunk = http.read_response_body_chunk()
		if chunk.size() == 0:
				# Got nothing, wait for buffers to fill a bit.
				OS.delay_usec(500)
		else:
				rb = rb + chunk
	if cancel: return ""
	var text = rb.get_string_from_ascii()
	return { "headers": headers, "code": code, "text": text }

func _data_to_multipart(boundaryKey: String, data_to_send) -> PoolByteArray:
	var boundary = '\r\n--'+boundaryKey+'\r\n'
	var boundaryEnd = '\r\n--'+boundaryKey+'--\r\n'
	var content = []
	for key in data_to_send:
		var value = str(data_to_send[key]) if data_to_send[key] else "nothing"
		content.append('Content-Disposition: form-data; name="'+key+'"'+'\r\n\r\n'+value)
	var body = boundary+PoolStringArray(content).join(boundary)+boundaryEnd
	return body.to_ascii()

# writes all the received data as multipart form data
func _make_post_request(data_to_send: Dictionary):
	if not _connect():
		return
	
	randomize()
	var key = str(randf() + OS.get_unix_time()).sha256_text()
	print("key: ", key)
	var boundaryKey = "---------------------------GodotFileUploadBoundaryZ29kb3RmaWxl"+key
	var body = _data_to_multipart(boundaryKey, data_to_send)

	var headers = [ 
		"Content-Length: "+String(body.size()),
		"Content-type: multipart/form-data; boundary="+boundaryKey
	]
	

	if http.request_raw(HTTPClient.METHOD_POST, url, headers, body) != OK:
		push_error("could not make request")
		return
	while http.get_status() == HTTPClient.STATUS_REQUESTING and not cancel:
		print("sending")
		http.poll()
		if not OS.has_feature("web"):
				OS.delay_msec(500)
		else:
				yield(Engine.get_main_loop(), "idle_frame")
	if cancel: return
	if http.get_status() != HTTPClient.STATUS_BODY and http.get_status() != HTTPClient.STATUS_CONNECTED:
		push_error("the request to `//"+server_url+':'+str(port)+url+"` was not processed")
		return
	print("the request to `//"+server_url+':'+str(port)+url+"` finished")
	call_deferred("_end")
	return _read_response()

func _end():
	print("response received")
	timer.stop()
	var response = thread.wait_to_finish()
	if response.code >= 400 || response.code == 0:
		emit_signal("errored", response)
	else:
		emit_signal("finished", response)

#func _exit_tree():
#	_end()
