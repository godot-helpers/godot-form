tool
################################################################################
#
# NORMALIZED LINE EDIT
# Just a normal LineEdit node, but with three differences:
# 1. it sends a "changed" signal on change
# 2. it uses `disabled` as a proxy for `editable`
# 3. you can set or get its text value through `value`
#
################################################################################

extends LineEdit
class_name LineEditNormalized, "./LineEdit.svg"

################################################################################
#
# SIGNALS HANDLING
# 

signal changed(value)

onready var _connected_to_self = connect("text_changed",self,"_on_text_changed")

func _on_text_changed(_value:String) -> void:
	emit_signal("changed", _value)


################################################################################
#
# VALUES NORMALIZING
# 

export(bool) var disabled setget set_disabled, get_disabled

func set_disabled(_value: bool) -> void:
	editable = not _value

func get_disabled() -> bool: 
	return not editable

var value setget set_value, get_value

func set_value(_value: String) -> void: 
	text = _value

func get_value() -> String: 
	return text
