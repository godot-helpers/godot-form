tool
################################################################################
#
# NORMALIZED CHECKBOX
# Just a normal CheckBox node, but with three differences:
# 1. it sends a "changed" signal on change
# 2. you can set or get its value through `value`
# 3. it has `placeholder_text` and `placeholder_alpha`. They do nothing, but they
#    exist so it is easy to set values in a loop
#
################################################################################

extends CheckBox
class_name CheckBoxNormalized, "./CheckBox.svg"

################################################################################
#
# SIGNALS HANDLING
# 

signal changed(value)

func _set(prop: String, value):
	if prop == "pressed":
		emit_signal("changed", value)

################################################################################
#
# VALUES NORMALIZING
# 

var value setget _set_value, _get_value
func _set_value(value: bool) -> void: pressed = value
func _get_value() -> bool: return pressed

################################################################################
#
# PLACEHOLDER HANDLING
# 
# This does NOTHING in a checkbox, but it's a blackhole property, there
# so it can be set in loops without checking "if field has property such and
# such"
#

const _DEFAULT_PLACEHOLDER_ALPHA = 0.6

var placeholder_text:=''
var placeholder_alpha := 0.6
