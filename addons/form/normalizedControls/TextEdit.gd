tool
################################################################################
#
# NORMALIZED TEXT EDIT
# Just a normal TextEdit node, but with three differences:
# 1. it sends a "changed" signal on change
# 2. it uses `disabled` as a proxy for `editable`
# 3. you can set or get its text value through `value`
# 4. you can set a `placeholder_text`, and set `placeholder_alpha` too
#
################################################################################

extends TextEdit
class_name TextEditNormalized, "./TextEdit.svg"

################################################################################
#
# SIGNAL DISPATCHING
#

signal changed(value)

onready var _connected_to_self = connect("text_changed",self,"_on_text_changed")

func _on_text_changed() -> void:
	var _value = text
	if _placeholder_text_label:
		if _value == "":
			_placeholder_text_label.visible = true
		else:
			_placeholder_text_label.visible = false
		emit_signal("changed", _value)

################################################################################
#
# NORMALIZING VALUES & DISABLED
#

export(bool) var disabled setget set_disabled, get_disabled

func set_disabled(_value: bool) -> void:
	readonly = _value

func get_disabled() -> bool: 
	return readonly

var value setget set_value, get_value

func set_value(_value: String) -> void: 
	text = _value

func get_value() -> String: 
	return text

################################################################################
#
# PLACEHOLDER HANDLING
#

const _DEFAULT_PLACEHOLDER_ALPHA = 0.6

var _placeholder_text_label: TextEdit

func has_text() -> bool:
	return text != ''

func is_empty() -> bool:
	return not has_text()

func _get_placeholder_text_node() -> TextEdit:
	if not _placeholder_text_label:
		_placeholder_text_label = TextEdit.new()
		_placeholder_text_label.modulate.a = _DEFAULT_PLACEHOLDER_ALPHA
		_placeholder_text_label.rect_size = rect_size
		_placeholder_text_label.visible = is_empty()
		_placeholder_text_label.mouse_filter = MOUSE_FILTER_IGNORE
		_placeholder_text_label.readonly = true
		add_child(_placeholder_text_label)
	return _placeholder_text_label

export(String) var placeholder_text setget _set_placeholder_text, _get_placeholder_text

func _set_placeholder_text(_value:String):
	var placeholder = _get_placeholder_text_node()
	placeholder.text = _value

func _get_placeholder_text() -> String:
	var placeholder = _get_placeholder_text_node()
	return placeholder.text

export(float, 0, 1) var placeholder_alpha = _DEFAULT_PLACEHOLDER_ALPHA setget _set_placeholder_alpha, _get_placeholder_alpha

func _set_placeholder_alpha(_value: float):
	var placeholder = _get_placeholder_text_node()
	placeholder.modulate.a = _value

func _get_placeholder_alpha() -> float:
	var placeholder = _get_placeholder_text_node()
	return placeholder.modulate.a

func _notification(what):
	if what == NOTIFICATION_RESIZED && _placeholder_text_label:
		_placeholder_text_label.rect_size = rect_size
