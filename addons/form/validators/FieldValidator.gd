tool
extends Node
class_name FormFieldValidator, "FieldValidator.svg"

const is_validator = true
const STATUS = preload('../fieldsComponents/Constants.gd').STATUS
export(String) var message = "" setget set_message, get_message

func _ready():
	set_physics_process(false)
	set_process(false)

func validate_value(_value, _is_required: bool) -> int:
	return STATUS.NONE

func set_message(msg: String) -> void:
	message = msg

func get_message() -> String:
	if message:
		return message
	else:
		return get_default_message()

# Override this in Validators to return a default message
func get_default_message() -> String:
	return ""

# This is called from the field to display to the user
# Use this method to augment the message, if necessary
func get_formatted_message() -> String:
	return get_message()

