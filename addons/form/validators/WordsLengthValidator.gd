tool
extends FormFieldValidator
class_name WordsLengthValidator

export(int,1) var minimum_words := 3
export(int,1) var minimum_word_characters := 3

func get_default_message() -> String:
	return "please enter at least {} words of {} letters each"

func get_formatted_message() -> String:
	var params = [ minimum_words, minimum_word_characters]
	return get_message().format(params,"{}")

func validate_value(value: String, is_required: bool) -> int:
	if value == "" and not is_required:
		return STATUS.NONE
	var words = value.replace("  "," ").split(" ")
	if words.size() >= minimum_words:
		var counter = 0
		for word in words:
			if word.length() >= minimum_word_characters:
				counter+=1
				if counter >= minimum_words:
					return STATUS.OK
	return STATUS.NO