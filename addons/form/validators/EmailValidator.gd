tool
extends FormFieldValidator
class_name EmailValidator

func get_default_message() -> String:
	return "your email is malformed"

func validate_value(value, is_required:bool) -> int:
	if value == "" and not is_required:
		return STATUS.NONE
	var valid = validate_email(value)
	var validity = STATUS.OK if valid else STATUS.NO
	return validity

func validate_email(text: String) -> bool:
	var atIndex = text.find("@")
	var dotIndex = text.find_last(".")
	var maxLength = text.length() -1
	var valid = atIndex > 0 and \
		atIndex < maxLength and \
		dotIndex > atIndex + 1 and \
		dotIndex < maxLength
	return valid
