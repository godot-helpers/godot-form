tool
extends FormFieldValidator
class_name CharactersLengthValidator

export(int,1) var minimum_length = 5
export(int,0) var maximum_length = 100000

func get_default_message() -> String:
	return "please enter at least {} characters"

func get_formatted_message() -> String:
	var params = [minimum_length]
	return get_message().format(params,"{}")

func validate_value(value: String, is_required: bool) -> int:
	if value == "" and not is_required:
		return STATUS.NONE
	if value.length() >= minimum_length and (maximum_length == 0 or value.length() <= maximum_length):
		return STATUS.OK
	return STATUS.NO
