tool
extends FormFieldValidator
class_name RequiredValidator

func get_default_message() -> String:
	return "This field is required"

func validate_value(value: String, _is_required: bool) -> int:
	if value:
		return STATUS.OK
	return STATUS.NO
