tool
extends Container
#class_name FlexContainer, "./FlexContainer.svg"

# This is too simplistic and doesn't work.
# It's kept here as a WIP
#

export(int) var breakpoint_width := 400
export(float) var margin = 5
var _previous_size = 0

func _notification(what):
	if what == NOTIFICATION_SORT_CHILDREN:
		_previous_size = get_size().x
		if rect_size.x <= breakpoint_width:
			print("now vertical")
			_order_vertically()
		else:
			print("now horizontal")
			_order_horizontally()

func _width_has_changed():
	var width = rect_size.x
	return \
		(width <= breakpoint_width and _previous_size > breakpoint_width) or \
		(width > breakpoint_width and _previous_size <= breakpoint_width)

func _order_vertically():
	var y = 0 
	for child in get_children():
		if not child is Control: continue
		var height = child.rect_size.y
		fit_child_in_rect( child, Rect2( Vector2(0,y), child.rect_size))
		y+= height + margin
	rect_size.y = y

func _order_horizontally():
	var x = 0 
	for child in get_children():
		if not child is Control: continue
		var width = child.rect_size.x
		fit_child_in_rect( child, Rect2( Vector2(x,0), child.rect_size))
		x+= width + margin
	rect_size.x = x
