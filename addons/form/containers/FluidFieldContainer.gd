extends VBoxContainer

export(int) var breakpoint_width := 400
var _previous_size = 0
onready var _container_horizontal: HBoxContainer = $HBoxContainer
onready var _container_vertical: VBoxContainer = self
onready var _container_current: Container = _container_vertical

func _ready():
	re_order()

func _notification(what):
	if what == NOTIFICATION_RESIZED and _width_has_changed():
		_previous_size = get_size().x
		re_order()

func re_order():
	var container: Container = _get_proper_container()
	if container == _container_current:
		return
	for child in _container_current.get_children():
		if child == container:
			continue
		_container_current.remove_child(child)
		container.add_child(child)
	_container_current = container

func _width_has_changed():
	var width = get_size().x
	return \
		(width <= breakpoint_width and _previous_size > breakpoint_width) or \
		(width > breakpoint_width and _previous_size <= breakpoint_width)

func _get_proper_container() -> Container:
	var width = get_size().x
	if width < breakpoint_width:
		return _container_vertical 
	else:
		return _container_horizontal
	
