extends VBoxContainer
class_name Form, "Form.svg"

################################################################################
#
# FORM
#
# A form component. It has an array of field_nodes that must be set.
# Those fields will be tracked for submission and changes
#
################################################################################

################################################################################
#
# FIELDS MANAGEMENT
#

const STATUS = preload("../fieldsComponents/Constants.gd").STATUS
export(Array, NodePath) var fields_nodes = []

var fields = {}
var is_valid = false

func _verify_node_is_field(node, path):
	if not OS.is_debug_build():
		return
	assert(node != null, "node '%s' not found"%[path])
	assert('value' in node, "node %s should have a `value` property"%[path])
	assert(node.has_signal('changed'), "node %s should have a \"changed\" signal"%[path])

func _on_ready_prepare_fields():
	for path in fields_nodes:
		var node: FormField = get_node(path)
		node.is_disabled = get_is_disabled()
		_verify_node_is_field(node, path)
		fields[node.name] = node
		node.connect("changed", self, "_field_changed", [node.name])

################################################################################
#
# CHANGES DISPATCHING
#

signal field_changed(field_name, value)

# When any tracked field changes, a signal is emitted
# This can be useful to autosave, or do other manipulation
func _field_changed(value, field_name: String):
	emit_signal("field_changed", field_name, value)

################################################################################
#
# SUBMIT BUTTON HANDLING
#

export(NodePath) var submit_button_node: NodePath
onready var submit_button: Button = get_node_or_null(submit_button_node)

func _on_ready_prepare_button() -> void:
	if submit_button_node:
		submit_button.connect("pressed", self, "on_submit_button_pressed", [submit_button])
		submit_button.disabled = get_is_disabled()

# This dispatches the submit event and _disables the form_
# Remember to re-enable it once you're done
func on_submit_button_pressed(button: Button) -> void:
	if validate():
		set_is_disabled(true)
		emit_signal("submit", serialize())

################################################################################
#
# VALIDATION AND SERIALIZATION HANDLING
#

# if true, serialization will skip empty fields
export(bool) var clean_serialized_values = true

# This sets a field's value from a dictionary. Useful to pre-fill a form
func deserialize(dict:Dictionary):
	for key in dict:
		if key in fields:
			fields[key].value = dict[key]

# This saves a form's data as a dictionary
func serialize() -> Dictionary:
	var dict: Dictionary = {}
	for key in fields:
		var field = fields[key]
		var value = field.value
		if clean_serialized_values and \
			( \
				(value is String and value == "") \
				or value == null\
			):
			continue
		dict[key] = value
	return dict

# Validates each tracked field
func validate():
	is_valid = true
	for key in fields:
		var field = fields[key]
		var value = field.validate()
		if value == STATUS.NO:
			is_valid = false
			return false
	return is_valid

################################################################################
#
# SUBMISSION HANDLING
#

# dispatches when a form submits. A form submits only if it is valid, so this
# signal ensures the dictionary has been validated
signal submit(dict)

# Call when the form is ready to submit; usually from a submit button
func submit():
	if validate(): emit_signal("submit", serialize())

################################################################################
#
# DISABLED HANDLING
#

export(bool) var is_disabled = false setget set_is_disabled, get_is_disabled 

func set_is_disabled(is_it:bool) -> void:
	is_disabled = is_it
	set_fields_disabled(is_it)
	if submit_button:
		submit_button.disabled = is_it

func get_is_disabled() -> bool:
	return is_disabled

# Sets all tracked fields to disabled/enabled
func set_fields_disabled(is_it: bool) -> void:
	for key in fields:
		var field: FormField = fields[key]
		field.is_disabled = is_it

################################################################################
#
# BOOTSTRAPPING
#

func _ready():
	_on_ready_prepare_fields()
	_on_ready_prepare_button()
