tool
extends VBoxContainer
class_name FormField, "FormField.svg"

###############################################################################
#
# HANDLING CHANGE
#

signal changed(value)

func _editor_check_up():
	if not OS.is_debug_build() or Engine.is_editor_hint():
		return
	assert(field != null, "field is null")
	# This will work in 4.0, see: https://github.com/godotengine/godot/pull/33508
	# assert(field.has_signal("changed"), "field ''%s' does not use a \"changed\" signal"%[field])
	# in the meantime:
	assert(field.get_signal_list()[0]["name"] == "changed", "field ''%s' does not use a \"changed\" signal"%[field])
	for prop in ['value', 'placeholder_text', 'disabled']:
		assert(prop in field, "field does not have a `%s` property"%[prop])
	if status_node:
		assert('status' in status_node, "Status node %s does not have a `status` property"%[status_node])

func _prepare_field():
	if Engine.is_editor_hint():
		return
	_editor_check_up()
	var _ok = field.connect("changed", self, "_on_field_changed")

func _on_field_changed(_value):
	emit_signal("changed", _value)
	_start_live_validation_timer()

###############################################################################
#
# PROPERTIES PROXIES
#

export(String) var title setget set_title, get_title

func set_title(_value: String) -> void:
	title_label.text = _value

func get_title() -> String: 
	return title_label.text if title_label else ''

export(String) var placeholder_text setget set_placeholder_text, get_placeholder_text

func set_placeholder_text(_value: String) -> void: 
	field.placeholder_text = _value

func get_placeholder_text() -> String:
	return field.placeholder_text if field else ''

export(String) var help_text setget set_help_text, get_help_text

func set_help_text(_value: String) -> void: 
	help_label.text = _value

func get_help_text() -> String:
	return help_label.text if help_label else ''

export(bool) var is_disabled setget set_is_disabled, get_is_disabled

func set_is_disabled(_value: bool) -> void:
	field.disabled = _value

func get_is_disabled() -> bool:
	return field.disabled if field else false

###############################################################################
#
# VALIDATION HANDLING
#

export(bool) var is_required = false
const STATUS = preload('../fieldsComponents/Constants.gd').STATUS
var validity = STATUS.NONE setget set_validity, get_validity
export(float, 0, 10) var first_validation_delay := 2.0
export(float, 0, 10) var validation_delay := 0.3
export(bool) var live_validation = false
var timer: Timer

func _prepare_timer():
	timer = Timer.new()
	timer.one_shot = true
	# warning-ignore:return_value_discarded
	timer.connect("timeout", self, "_live_validation")
	timer.autostart = false
	timer.stop()
	add_child(timer)

signal validity(validity)

var validators = []
var validators_cached = false

# Builds a cache of all validators that are direct descendants of the field
# This is not expected to change, so doing it once and caching it should
# be enough
func _get_validators():
	if not validators_cached:
		validators = []
		for node in get_children():
			if 'is_validator' in node:
				validators.push_back(node)
		validators_cached = true
	return validators

func _validate_value() -> int:
	var _value = get_value()
	var ret = STATUS.NONE
	for validator in _get_validators():
		var result = validator.validate_value(_value, is_required)
		if result == STATUS.NO:
			add_error(validator.name, validator.get_formatted_message())
			ret = result
		else:
			remove_error(validator.name)
		if result == STATUS.OK and ret != STATUS.NO:
			ret = result
	return ret

func _validate_value_and_emit():
	var new_validity = _validate_value()
	if new_validity != validity:
		set_validity(new_validity)
		emit_signal("validity", validity)

func _live_validation():
	if live_validation:
		_validate_value_and_emit()

func _start_live_validation_timer():
	if timer:
		timer.start(first_validation_delay + validation_delay)
		first_validation_delay = 0

func set_validity(new_validity: int) -> void:
	validity = new_validity
	if status_node:
		status_node.status = validity

func get_validity() -> int:
	return validity

func validate() -> int:
	var new_validity = _validate_value()
	if new_validity != validity:
		set_validity(new_validity)
	return validity

###############################################################################
#
# FEEDBACK HANDLING
#
# Feedback is dispatched as messages. Any node can intercept those messages
# and display them, or do whatever it wants with them
#

signal add_message(id, message, message_type)
signal remove_message(id)

func add_message(id: String, message:String, message_type:int = STATUS.NONE):
	if messages_node: 
		messages_node.add_message(id, message, message_type)
	emit_signal("add_message", id, message, message_type)

func remove_message(id: String):
	if messages_node: 
		messages_node.remove_message(id)
	emit_signal("remove_message", id)

func add_error(id:String, message:String):
	add_message(id, message, STATUS.NO)

func remove_error(id: String):
	remove_message(id)

###############################################################################
#
# CONSTANTS FOR TYPING
#

const MessagesContainer = preload("FormFieldMessagesContainer.gd")
const FormFieldStatus = preload("FormFieldStatus.tscn")

###############################################################################
#
# SUB NODES PATHS
#

var title_label: Label
var field: Control
var help_label: Label
var status_node: Node
var messages_node: MessagesContainer

# This creates the field. Override to instanciate or return a different field
func _prepare_node_tree_field() -> Control:
	var field = preload("../normalizedControls/LineEdit.gd").new()
	field.size_flags_horizontal = SIZE_EXPAND_FILL
	return field

# This creates the status node
func _prepare_node_tree_status_node() -> Node:
	return FormFieldStatus.instance()

# This creates the field container. It's expected to contain the field, and the
# status node
func _prepare_node_tree_field_container(children: Array) -> Container:
	var fieldInputContainer = HBoxContainer.new()
	fieldInputContainer.size_flags_horizontal = SIZE_EXPAND_FILL

	for child in children:
		fieldInputContainer.add_child(child)

	return fieldInputContainer


# Prepares and returns an instance of the title label node
func _prepare_node_tree_title_label() -> Control:
	var _title = Label.new()
	_title.size_flags_vertical = SIZE_SHRINK_CENTER
	return _title

# This creates the main container. This container typically contains:
# - the title
# - the field container (generated in _prepare_node_tree_field_container)
# But it could contain anything you pass it
func _prepare_node_tree_main_container(children: Array) -> Container:
	var fieldContainer = VBoxContainer.new()
	
	for child in children:
		fieldContainer.add_child(child)
	return fieldContainer

# Prepares and returns an instance of the help label node
func _prepare_node_tree_help_label() -> Control:
	help_label = Label.new()
	help_label.size_flags_vertical = SIZE_SHRINK_CENTER
	help_label.modulate.a = .5
	return help_label

# Prepares and returns an instance of the messages node
func _prepare_node_tree_messages() -> Control:
	return MessagesContainer.new()

# This method is called on init, and prepares every sub-node, which are:
# - field
# - title_label
# - help_label
# - messages
# They do not get appended as children, only created and assigned
func _prepare_node_tree_assign_nodes() -> void:
	field = _prepare_node_tree_field()
	title_label = _prepare_node_tree_title_label()
	help_label = _prepare_node_tree_help_label()
	messages_node = _prepare_node_tree_messages()
	status_node = _prepare_node_tree_status_node()

# This creates all the sub-nodes
func _on_init_prepare_node_tree():

	_prepare_node_tree_assign_nodes()

	var field_container = _prepare_node_tree_field_container([field, status_node])
	var main_container = _prepare_node_tree_main_container([title_label, field_container])

	add_child(main_container)
	add_child(help_label)
	add_child(messages_node)

###############################################################################
#
# VALUES
#

export(String) var text_value setget set_field_text_value, get_field_text_value

func set_field_text_value(_value: String) -> void: 
	field.value = str(_value)

func get_field_text_value() -> String:
	if value == false or value == true or value == null:
		return ''
	return str(value) 

export(bool) var boolean_value setget set_field_boolean_value, get_field_boolean_value

func set_field_boolean_value(_value: bool) -> void: 
	field.value = bool(_value)

func get_field_boolean_value() -> bool:
	if field.value is String:
		if field.value == "": 
			return false
		return true
	if field.value is bool:
		return field.value
	return false

var value setget set_value, get_value

func set_value(_value):
	field.value = _value

func get_value():
	return field.value if field else null

###############################################################################
#
# BOOTSTRAPPING
#

func _init():
	_on_init_prepare_node_tree()

func _ready():
	_prepare_timer()
	_prepare_field()
