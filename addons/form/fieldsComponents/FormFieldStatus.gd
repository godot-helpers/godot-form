tool
extends Panel

const STATUS = preload('./Constants.gd').STATUS
export(STATUS) var status = STATUS.NONE setget set_status, get_status

func set_status(value: int) -> void:
	if value == status:
		return
	if status != STATUS.NONE:
		if status == STATUS.OK:
			$AnimationPlayer.queue("ok-out")
		if status == STATUS.NO:
			$AnimationPlayer.queue("no-out")
	if value != STATUS.NONE:
		if value == STATUS.OK:
			$AnimationPlayer.queue("ok-in")
		if value == STATUS.NO:
			$AnimationPlayer.queue("no-in")
	status = value
	
func get_status() -> int:
	return status
