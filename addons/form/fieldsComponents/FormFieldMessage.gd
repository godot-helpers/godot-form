tool
extends HBoxContainer

const STATUS = preload('./Constants.gd').STATUS
const ok = preload('../icons/ok.svg')
const no = preload('../icons/no.svg')

export(STATUS) var status: int = STATUS.OK setget _set_status, _get_status

func _set_status(value:int) -> void:
	if not is_inside_tree(): yield(self, 'ready')
	if $Icon == null: return
	status = value
	if value == STATUS.OK:
		$Icon.texture = ok
	elif value == STATUS.NO:
		$Icon.texture = no
	else:
		$Icon.texture = null

func _get_status() -> int:
	return status

export(String) var text setget _set_text, _get_text

func _set_text(value: String) -> void:
	if not is_inside_tree(): yield(self, 'ready')
	if $Label == null: return
	$Label.text = value

func _get_text() -> String:
	return $Label.text

export(float, 0, 1) var alpha = 0.6 setget _set_alpha, _get_alpha

func _set_alpha(value: float) -> void:
	if not is_inside_tree(): yield(self, 'ready')
	if $Label == null: return
	$Label.modulate.a = value

func _get_alpha() -> float:
	return $Label.modulate.a

export(Color) var color = Color(1,1,1,1) setget _set_color, _get_color

func _set_color(value: Color) -> void:
	if not is_inside_tree(): yield(self, 'ready')
	if $Label == null: return
	$Label.set("custom_colors/font_color", value)

func _get_color() -> Color:
	return $Label.get("custom_colors/font_color")
