extends VBoxContainer
class_name FormFieldMessagesContainer, "FormFieldMessagesContainer.svg"

const STATUS = preload('./Constants.gd').STATUS
const FormFieldMessage = preload('./FormFieldMessage.tscn')

export(Color) var error_color = Color(1,.2,.2,1)

var messages = {}

# Appends a message to the stack
func add_message(id:String, message_text:String, message_status:int = 0):
	if messages.has(id):
		return
	var message = FormFieldMessage.instance()
	message.status = message_status
	message.text = message_text
	if message_status == STATUS.NO:
		message.color = error_color
	messages[id] = message
	add_child(message)

# Removes a message from the stack
func remove_message(id):
	if messages.has(id):
		var node = messages.get(id)
		remove_child(node)
		messages.erase(id)
		node.queue_free()
