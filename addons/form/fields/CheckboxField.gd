tool
extends FormField
class_name FormCheckboxField, "CheckboxField.svg"


###############################################################################
#
# OVERRIDING
#

func set_field_boolean_value(_value: bool) -> void: 
	field.value = _value

func get_field_boolean_value() -> bool:
	return field.value

# We override this to cancel creating a status node, which makes no sense
# for a checkbox field
func _prepare_node_tree_status_node() -> Node:
	return null

# We override this method to return a Checkbox field rather than a regular LineEdit
func _prepare_node_tree_field() -> Control:
	var field: CheckBox = preload("../normalizedControls/CheckBox.gd").new()
	return field

# We override this method to put the help text before the text field
func _on_init_prepare_node_tree():

	_prepare_node_tree_assign_nodes()

	var field_container = _prepare_node_tree_field_container([field, title_label])
	var main_container = _prepare_node_tree_main_container([field_container, help_label])

	add_child(main_container)
	add_child(messages_node)