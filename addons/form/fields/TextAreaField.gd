tool
extends FormField
class_name FormTextAreaField, "TextAreaField.svg"

###############################################################################
#
# ADDITIONAL SETTERS
#

export(float) var text_box_height setget set_text_box_height, get_text_box_height

func set_text_box_height(h: float) -> void:
	field.rect_min_size.y = h

func get_text_box_height() -> float:
	return field.rect_min_size.y

###############################################################################
#
# OVERRIDING
#

# We override this method to return a TextEdit field rather than a regular LineEdit
func _prepare_node_tree_field() -> Control:
	var field: TextEdit = preload("../normalizedControls/TextEdit.gd").new()
	field.size_flags_horizontal = SIZE_EXPAND_FILL
	return field

# We override this method to put the help text before the text field
func _on_init_prepare_node_tree():

	_prepare_node_tree_assign_nodes()

	var field_container = _prepare_node_tree_field_container([field, status_node])
	var main_container = _prepare_node_tree_main_container([title_label, help_label, field_container])

	add_child(main_container)
	add_child(messages_node)