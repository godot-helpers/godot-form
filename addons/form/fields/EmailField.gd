tool
extends FormField
class_name FormEmailField, "EmailField.svg"

###############################################################################
#
# OVERRIDING
#

# We override this method to automatically append an email validator
func _on_init_prepare_node_tree():
	._on_init_prepare_node_tree()
	var email_validator = preload("../validators/EmailValidator.gd").new()
	add_child(email_validator)
