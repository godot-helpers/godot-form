tool
extends EditorPlugin

func _enter_tree():
	add_autoload_singleton("SystemInfo","res://addons/system-info/system-info.gd")
	
func _exit_tree():
	remove_autoload_singleton("SystemInfo")
