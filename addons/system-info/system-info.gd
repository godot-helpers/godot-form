extends Node

var is_joypad = false
var joypad_index = -1

func _init():
	var _err = Input.connect("joy_connection_changed", self, "_on_joy_connection_changed")

func get_info():
	var screen_size_vec = OS.get_screen_size()
	var datetime = OS.get_datetime(true)
	var info = {
		"os-name": OS.get_name(),
		"model": OS.get_model_name(),
		"collection_date": str(datetime.year) + '-' + str("%02d" % [datetime.month]) + '-' + str("%02d" % [datetime.day]) + ' ' + str("%02d" % [datetime.hour]) + ':' + str("%02d" % [datetime.minute]) + ':' + str("%02d" % [datetime.second]),
		"video_driver": OS.get_video_driver_name(OS.get_current_video_driver()),
		"video_adapter": VisualServer.get_video_adapter_name(),
		"video_vendor": VisualServer.get_video_adapter_vendor(),
		"screen_size": str(screen_size_vec.x)+'x'+str(screen_size_vec.y) ,
		"screen_dpi": OS.get_screen_dpi(),
		"cores": OS.get_processor_count(),
		"locale": OS.get_locale(),
		"joypad": Input.get_joy_name(joypad_index) if is_joypad else ""
	}
	return info

func _on_joy_connection_changed(device_id, connected):
	is_joypad = connected
	joypad_index = device_id

func _take_screenshot() -> Image:
	var image: Image = get_viewport().get_texture().get_data()
	image.flip_y()
	return image

func augment(data: Dictionary, with_screenshot: bool = false) -> Dictionary:
	var info = get_info()
	for key in data:
		info[key] = data[key]
	if with_screenshot:
		#info.screenshot = _take_screenshot()
		pass
	return info