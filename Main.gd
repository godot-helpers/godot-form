extends Node

################################################################################
#
# CACHING CHILDREN FOR READABILITY
#

onready var take_screenshot_checkbox = $WindowDialog/Form/MarginContainer/Form/HBoxContainer/take_screenshot
onready var poster: FormPoster = $FormPoster
onready var window: WindowDialog = $WindowDialog
onready var form: Form = $WindowDialog/Form/MarginContainer/Form
onready var notification: AcceptDialog = $AcceptDialog

################################################################################
#
# BUTTONS HANDLING
#

func _on_Form_submit(dict):
	var take_screenshot = take_screenshot_checkbox.value
	var data = SystemInfo.augment(dict, take_screenshot)
	poster.submit(data, 5)

func _on_Button_pressed():
	window.popup_centered()

################################################################################
#
# SUBMISSION HANDLING
#

func _on_ready_prepare_form_poster():
	if OS.is_debug_build():
		poster.server_url = "127.0.0.1"
		poster.port = 3000
		poster.use_https = false
		poster.url = "/"
	assert(poster.connect("canceled", self, "_on_form_canceled") == OK)
	assert(poster.connect("errored", self, "_on_form_errored") == OK)
	assert(poster.connect("finished", self, "_on_form_finished") == OK)

func _on_form_canceled():
	show_notification("Error!", "The connection timed out")
	form.is_disabled = false

func _on_form_errored(response):
	show_notification("Error!", "An unknown error occured")
	push_error(response)
	form.is_disabled = false

func _on_form_finished(_response):
	show_notification("Thank you!", "Your answer has been recorded")
	form.is_disabled = false

func show_notification(title, text):
	notification.window_title = title
	notification.dialog_text = text
	notification.popup_centered()

################################################################################
#
# BOOSTRAP
#

func _ready():
	window.popup_centered()
	_on_ready_prepare_form_poster()
